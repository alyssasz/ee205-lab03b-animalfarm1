###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @Alyssa Zhang <alyssasz@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   Jan 31 2021
###############################################################################

CC     = gcc
CFLAGS = -g

TARGET = animalfarm 

all: animalfarm

main.o: main.c animals.h cat.h 
	$(CC) $(CFLAGS) -c main.c

animals.o: animals.c animals.h
	$(CC) $(CFLAGS) -c animals.c

cat.o: cat.c cat.h
	$(CC) $(CFLAGS) -c cat.c

animalfarm: main.o animals.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o animals.o cat.o

clean:
	rm -f *.o animalfarm
