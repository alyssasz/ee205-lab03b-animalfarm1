///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   Jan 31 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Gender into strings for printf()
char* genderName (enum Gender gender) {

   char* Gender; 

   switch (gender) {
      
         case 0: Gender = "Male"; 
                  break;

         case 1: Gender = "Female";
                  break; 
   }

   return Gender;
   return NULL;

}


/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {
   
   char* Color;
   
   switch (color) {
      
      case 0: Color = "Black"; 
               break;

      case 1: Color = "White";
               break; 

      case 2: Color = "Red";
               break; 

      case 3: Color = "Blue";
               break; 

      case 4: Color = "Green";
               break; 

      case 5: Color = "Pink";
               break;  

   }
   
   return Color;
   return NULL; 
};

/// Decode the enum CatBreeds into strings for printf()
char* catbreedsName (enum CatBreeds catbreeds) {
   
   char* CatBreeds;
   
   switch (catbreeds) {
      
      case 0: CatBreeds = "Main Coon"; 
               break;

      case 1: CatBreeds = "Manx";
               break; 

      case 2: CatBreeds = "Short Hair";
               break; 

      case 3: CatBreeds = "Persian";
               break; 

      case 4: CatBreeds = "Sphynx";
               break; 

   }
   
   return CatBreeds;
   return NULL; 
};


